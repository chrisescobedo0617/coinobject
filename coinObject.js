const coin = {
    state: 0,
    flip: function() {
        // 1. One point: Randomly set your coin object's "state" property to be either 
        //    0 or 1: use "this.state" to access the "state" property on this object.
        let randomFlip = Math.floor(Math.random() * 2)
        this.state = randomFlip
        return this.state
    },
    toString: function() {
        // 2. One point: Return the string "Heads" or "Tails", depending on whether
        //    "this.state" is 0 or 1.
        let headsOrTails = ""
        if (this.state === 0) {
            headsOrTails = "Heads"
        } else {headsOrTails = "Tails"}
        return headsOrTails
    },
    toHTML: function() {
        const image = document.createElement('img');
        // 3. One point: Set the properties of this image element to show either face-up
        //    or face-down, depending on whether this.state is 0 or 1.
        coinCurrentFace = this.state
        if (coinCurrentFace === 0) {
            image.setAttribute("src", "imgs/coinObject.jpeg")
            image.setAttribute("height", 150)
            document.body.appendChild(image)
        } else {
            image.setAttribute("src", "imgs/download.jpeg"); 
            document.body.appendChild(image); 
            image.setAttribute("height", 150)
        }
        return image
    }
};

function createSpan(text){
    let newSpan = document.createElement('span')
    newSpan.innerHTML = text
    document.body.appendChild(newSpan)
}

function createBreak(){
    let newBreak = document.createElement('br')
    document.body.appendChild(newBreak)
}

function creatediv(){
    let newdiv = document.createElement('div')
    document.body.appendChild(newdiv)
  }

function display20Flips() {
    const results = [];
    // 4. One point: Use a loop to flip the coin 20 times, each time displaying the result of the flip as a string on the page.  After your loop completes, return an array with the result of each flip.
    for (let flips = 0; flips <= 20 ; flips++) {
        coin.flip()
        createSpan(coin.toString() + " ")
        createBreak()
        results.push(coin.state)
    }
    return results
}


function display20Images() {
    const results = [];
    // 5. One point: Use a loop to flip the coin 20 times, and display the results of each flip as an image on the page.  After your loop completes, return an array with result of each flip.
    for (let flips = 0; flips <= 20 ; flips++) {
        coin.flip()
        creatediv(coin.toHTML())
        results.push(coin.state)
    }
    return results
}

display20Flips()
display20Images()
